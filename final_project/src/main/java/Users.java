import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "Users")
public class Users implements Serializable {
    public Users(){

    }
    public Users(String name, String lastname, String username, String password, int access_query) {
        Username = username;
        Password = password;
        Name = name;
        Last_Name = lastname;
        Access_Query = access_query;
    }

    @Id
    @Column(name = "idUsers")
    @GeneratedValue(generator = "incrementor")
    @GenericGenerator(name = "incrementor", strategy = "increment")
    private int idUsers;
    @Column(name = "Username")
    private String Username;
    @Column(name = "Password")
    private String Password;
    @Column(name = "Name")
    private String Name;
    @Column(name = "Last_name")
    private String Last_Name;
    @Column(name = "Access_Query")
    private int Access_Query;

    public int getIdUsers() {
        return idUsers;
    }

    public void setIdUsers(int idUsers) {
        this.idUsers = idUsers;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        Username = username;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getLast_Name() {
        return Last_Name;
    }

    public void setLast_Name(String last_Name) {
        Last_Name = last_Name;
    }

    public int getAccess_Query() {
        return Access_Query;
    }

    public void setAccess_Query(int access_Query) {
        Access_Query = access_Query;
    }



}
