import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

public class checkAccess {
    public boolean CheckAccess(String username){
        int solution = 0;
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("org.hibernate.tutorial.jpa");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        List<Users> userList  = entityManager.createNativeQuery("SELECT * FROM Users WHERE Username = '" +
                username + "' AND Access_Query = 1 ;", Users.class).getResultList();
        if (userList.size() == 0) {
            solution = 1;
        }
        entityManagerFactory.close();
        if (solution == 1){
            return false;
        }

        return true;

    }
}
